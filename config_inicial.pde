class menu {



  void config_inicial() {

    fill(verde_escuro);
    noStroke();
    rect(0, 0, width, height);

    textAlign(CENTER);
    textSize(30);
    fill(verde_escuro);
    noStroke();
    rect(0, 0, width, 55);
    fill(0);
    text("FRIC FRAC", width/2, 40);





    // SELECIONAR O PLAYER A SER RENOMEADOR
    if (mouseX > width/2-175 && mouseX < width/2+175  && mouseY > 265 && mouseY < 310 && mousePressed) {
      selected_player_config = 1;
    }
    if (selected_player_config == 1) {
      strokeWeight(2);
      stroke(laranja);
    }
    fill(0); // Define a cor do texto para ciano (#36EADD)
    text("Nome do Player 1:", width/2-350, 300);
    fill(255); // Define a cor do texto para branco (255)
    rect(width/2-175, 260, 350, 50, 10);
    fill(0);
    text(name_playerUm, width/2, 297);

    strokeWeight(0);

    if (mouseX > width/2-175 && mouseX < width/2+175  && mouseY > 321 && mouseY < 375 && mousePressed) {
      selected_player_config = 2;
    }

    if (selected_player_config == 2) {
      strokeWeight(2);
      stroke(laranja);
    }


    text("Nome do Player 2:", width/2-350, 347);
    fill(255); // Define a cor do texto para branco (255)
    rect(width/2-175, 315, 350, 50, 10);
    fill(0);
    text(name_playerDois, width/2, 350);
    strokeWeight(0);





    // BOTAO CONFIRMAR
    fill(laranja);
    image(btn_menu, width/2-200, 470, 400, height/8 );
    //rect(width/2, 430, 350, 50, 10);
    fill(255);
    text("AVANÇAR", width/2, 540);

    if (mouseX > width/2-175 && mouseX < width/2+175  && mouseY > 508 && mouseY < 555) {

    fill(laranja);
    text("AVANÇAR", width/2, 540);
      if (mousePressed) {
        selected_player_config = 0;
        show_menu = true;
        show_config_inicial = false;
      }
    }



    fill(0, 100);
    rect(0, 100, width, 145);
    textSize(22);
    fill(#FF8200);
    text("REGRA:", width/2, 130);
    fill(255);
    text("1 - Cada jogador joga uma vez quando o seu lado for sinalizado.", width/2-width/3, 145, width/2+200, 100);
    
    text("2 - Para jogar, basta arrastar cada pedra para dentro das casas.", width/2-width/3, 175, width/2+200, 100);
    
    text("3 - O objetivo é alinhar 3 pedras na Vertical, horizontal ou diagonal.", width/2-width/3, 205, width/2+200, 100);
  }
}
