class config {



  void config_geral() {

    fill(verde_escuro);
    noStroke();
    rect(0, 0, width, height);

    textAlign(CENTER);
    textSize(30);
    fill(verde_escuro);
    noStroke();
    rect(0, 0, width, 55);
    fill(0);
    text("FRIC FRAC", width/2, 40);





    // SELECIONAR O PLAYER A SER RENOMEADOR
    if (mouseX > width/2-175 && mouseX < width/2+175  && mouseY > 265 && mouseY < 310 && mousePressed) {
      selected_player_config = 1;
    }
    if (selected_player_config == 1) {
      strokeWeight(2);
      stroke(laranja);
    }
    fill(0); // Define a cor do texto para ciano (#36EADD)
    text("Nome do Player 1:", width/2-350, 300);
    fill(255); // Define a cor do texto para branco (255)
    rect(width/2-175, 260, 350, 50, 10);
    fill(0);
    text(name_playerUm, width/2, 297);

    strokeWeight(0);

    if (mouseX > width/2-175 && mouseX < width/2+175  && mouseY > 321 && mouseY < 375 && mousePressed) {
      selected_player_config = 2;
    }

    if (selected_player_config == 2) {
      strokeWeight(2);
      stroke(laranja);
    }


    text("Nome do Player 2:", width/2-350, 347);
    fill(255); // Define a cor do texto para branco (255)
    rect(width/2-175, 315, 350, 50, 10);
    fill(0);
    text(name_playerDois, width/2, 350);
    strokeWeight(0);


    text("Trilha sonora:", width/2-385, 447);
    if(player_trilha_on == true){
      status_player = #15A32A;
      text_btn_player = "STOP";
    }else{
     status_player = #FF2121;
      text_btn_player = "PLAY";
    }
    fill(status_player); 
    strokeWeight(0);



    // BOTAO PLAYER
    image(btn_menu, width/2-200, 390, 400, height/8 );
    //rect(width/2, 430, 350, 50, 10);
    fill(255);
    text(text_btn_player, width/2, 440);

    if (mouseX > width/2-175 && mouseX < width/2+175  && mouseY > 408 && mouseY < 455) {

    fill(laranja);
    text(text_btn_player, width/2, 440);
      if (player_trilha_on == true && mousePressed) {
          player_trilha.pause();
          player_trilha_on = false;
      }else if (player_trilha_on == false && mousePressed){
         player_trilha.loop();
          player_trilha_on = true;
      }
    }


    // BOTAO CONFIRMAR
    fill(laranja);
    image(btn_menu, width/2-200, 490, 400, height/8 );
    //rect(width/2, 430, 350, 50, 10);
    fill(255);
    text("Confirmar", width/2, 540);

    if (mouseX > width/2-175 && mouseX < width/2+175  && mouseY > 508 && mouseY < 555) {

    fill(laranja);
    text("Confirmar", width/2, 540);
      if (mousePressed) {
        selected_player_config = 0;
        show_menu = true;
        show_config_geral = false;
      }
    }



  }
}


void keyPressed() {

  if (selected_player_config == 1) {
    fill(laranja);
    if (key == '\n' || key == '\r') { // Se a tecla pressionada for Enter
      // Limpa a entrada se a tecla pressionada for Enter
    } else if (key == BACKSPACE) { // Se a tecla pressionada for Backspace
      // Remove o último caractere da entrada
      if (name_playerUm.length() > 0) {
        name_playerUm = name_playerUm.substring(0, name_playerUm.length() - 1);
      }
    } else {
      // Adiciona a tecla pressionada à entrada
      name_playerUm = name_playerUm + key;
    }
  } else if (selected_player_config == 2) {


    if (key == '\n' || key == '\r') { // Se a tecla pressionada for Enter
      // Limpa a entrada se a tecla pressionada for Enter
    } else if (key == BACKSPACE) { // Se a tecla pressionada for Backspace
      // Remove o último caractere da entrada
      if (name_playerDois.length() > 0) {
        name_playerDois = name_playerDois.substring(0, name_playerDois.length() - 1);
      }
    } else {
      // Adiciona a tecla pressionada à entrada
      name_playerDois = name_playerDois + key;
    }
  }
}
