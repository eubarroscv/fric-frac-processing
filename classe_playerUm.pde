class playerUm {

  float inicio = 320;
  float fim = 40;
  boolean abrir = false;
  boolean fechar = true;
  boolean vez_playerUm = true;



  void playerUm_pedra(float x_position, float y_position) {

    fill(creme);
    stroke(verde_claro);
    strokeWeight(8);
    int tl = translate_center;
    triangle(tl + x_position-30, y_position + 30 , tl + x_position, y_position -25, tl + x_position + 30, y_position + 30);

  }
  
 


  void animar_playerUm() {
    //Animar player
    if (fechar == true && inicio == 360) {
      abrir = true;
      fechar = false;
    } else if (abrir == true && inicio == 340) {
      abrir = false;
      fechar = true;
    }

    if (selected_player == 1) {
      if (fechar == true) {
        inicio = inicio + 4;
        fim = fim - 4;
      } else {
        inicio = inicio - 4;
        fim = fim + 4;
      }
    }
  }


  // TITULO
  void player_playerUm_title() {
    if (game_start == true) {
    textSize(16);
    fill(0);
    text(name_playerUm, 80, 125);
    }
  }
}
