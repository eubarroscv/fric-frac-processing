//Definão das classes


splash_creen sp;
extrutura ex;
casas ca;
playerUm pm;
playerDois co;
pontuacao pt;
game_engine ge;
menu mn;
config cf;




//import processing.sound.*;
//import controlP5.*;
import ddf.minim.*;
import controlP5.*;

Minim minim;
AudioPlayer player_trilha;
AudioPlayer player_audiofx1;
AudioPlayer player_audiofx2;





// Definicao de variaveis

int num_jogada_player_1 = 0;
int num_jogada_player_2 = 0;

int translate_center;
boolean tabuleiro_center;

//Variavel de posição inicial X e Y de cada pedra
float p1x = 50, p1y = 190;
float p2x = 50, p2y = 250;
float p3x = 50, p3y = 310;

float p4x = 450, pay = 190;
float p5x = 450, pby = 250;
float p6x = 450, pcy = 310;


//Variavel de correção de posição das pedras ao serem carregadas
float xOffset = 0.0;
float yOffset = 0.0;

int caso_de_pontuacao = 0;


// Posição central X e Y de cada casa
int casa_a_x = 50,
  casa_a_y = 190,
  casa_b_x = 50,
  casa_b_y = 155,
  casa_c_x = 50,
  casa_c_y = 315;

int casa_d_x = 450,
  casa_d_y = 190,
  casa_e_x = 450,
  casa_e_y = 155,
  casa_f_x = 450,
  casa_f_y = 315;

int casa_1_x = 150,
  casa_1_y = 150,
  casa_2_x = 250,
  casa_2_y = 150,
  casa_3_x = 350,
  casa_3_y = 150;

int casa_4_x = 150,
  casa_4_y = 250,
  casa_5_x = 250,
  casa_5_y = 250,
  casa_6_x = 350,
  casa_6_y = 250;

int casa_7_x = 150,
  casa_7_y = 350,
  casa_8_x = 250,
  casa_8_y = 350,
  casa_9_x = 350,
  casa_9_y = 350;

// Variaveis de seleção
int selected_casa = 0;
int selected_pedra = 0;
int selected_player = 0;

int i = 0;

// Variaveis do estado do jogo
boolean game_start = false;
boolean show_ranking = false;
boolean pontuacao_efetuado = false;
boolean pausar_jogo = false;
int counter_down = 10;
boolean nova_jogada = false;
boolean debug = false;
boolean show_config_inicial = true;
boolean show_splash_screen = true;
boolean show_config_geral = false;
boolean show_menu = false;
boolean player_trilha_on = true;


int playerUm_size = 50;

//Variavel de indicação de qual jogador ocupou cada casa (0 indica que ainda nenhum jogador ocupou a casa)
int  casa_1_com_player = 0;
int  casa_2_com_player = 0;
int  casa_3_com_player = 0;
int  casa_4_com_player = 0;
int  casa_5_com_player = 0;
int  casa_6_com_player = 0;
int  casa_7_com_player = 0;
int  casa_8_com_player = 0;
int  casa_9_com_player = 0;

int  casa_1_com_pedra = 0;
int  casa_2_com_pedra = 0;
int  casa_3_com_pedra = 0;
int  casa_4_com_pedra = 0;
int  casa_5_com_pedra = 0;
int  casa_6_com_pedra = 0;
int  casa_7_com_pedra = 0;
int  casa_8_com_pedra = 0;
int  casa_9_com_pedra = 0;

String name_playerUm = "PLAYER UM";
String name_playerDois = "PLAYER DOIS";
String text_btn_player;
int selected_player_config = 0;

//Variavel de armazenamento de pontuacao de cada jogador
int player_Dois_pontuacao = 0;
int player_Um_pontuacao = 0;

//Variavel que regista as casas utilizadas

boolean casa_1_usada = false;

// Variaveis de cor
int
  branco = #ffffff,
  verde_escuro = #15A38D,
  verde_claro = #84D2C5,
  verde = #3ED2BA,
  laranja = #FF8B14,
  amarelo = #FFB803,
  status_player,
  creme = #F5EDDC;
PImage btn_menu, icon_config, bg;
PImage arrow_cursor;
PImage hand_cursor;
PFont theNeueBlack;

void setup() {

  surface.setTitle("Fric Frac 1.0"); // set window title
  //size(1780, 720);
  size(1200, 720);
  fullScreen();
  surface.setResizable(true);

  theNeueBlack = createFont("assets/font/TheNeue-Black.ttf", 50);
  textFont(theNeueBlack);

  minim = new Minim(this);
  player_trilha = minim.loadFile("assets/audio/swing2.mp3");
  player_audiofx1 = minim.loadFile("assets/audio/success2.mp3");
  player_audiofx2 = minim.loadFile("assets/audio/beep.mp3");
  player_trilha.loop();


  btn_menu = loadImage("assets/img/btn_menu.png");
  icon_config = loadImage("assets/img/icon_config.png");
  bg = loadImage("assets/img/bg.png");


  //Cursor
  arrow_cursor = loadImage("assets/img/arrow_cursor.png");
  hand_cursor = loadImage("assets/img/hand_cursor.png");
  
  sp = new splash_creen();
  ex = new extrutura();
  ca = new casas();
  mn = new menu();
  cf = new config();
  pm = new playerUm();
  co = new playerDois();
  pt = new pontuacao();
  ge = new game_engine();
  
}

void stop() {
  player_trilha.close();
  minim.stop();
  super.stop();
}


void draw() {
  frameRate(15);
  cursor(arrow_cursor, 30, 30);

  translate_center = width/2-250;
  strokeWeight(2);
  background(creme); 
  
      image(bg, width-300, height-500, 300, 500);

  if (tabuleiro_center == true) {
    translate(width/2, 0);
  } else {
    translate(0, 0);
  }


  
  ex.topBar();
  ex.linhas_casas();
  ca.selected_casa();

  ge.player_time(selected_player);

  //PONTOS
  pt.calc_pontuacao();

  pm.player_playerUm_title();
  co.player_playerDois_title();


  pm.playerUm_pedra(p1x, p1y);
  pm.playerUm_pedra(p2x, p2y);
  pm.playerUm_pedra(p3x, p3y);
  pm.animar_playerUm();


  co.playerDois_pedra(p4x, pay);
  co.playerDois_pedra(p5x, pby);
  co.playerDois_pedra(p6x, pcy);
  co.animar_playerDois();


  if (game_start == false) {
    ge.start_menu();
  } else {

    counter_down = counter_down - 1;
    if (counter_down == 0) {
      selected_player = 1;
    }
  }


  if (show_config_inicial == true) {
    mn.config_inicial();
  }
  
  if(show_config_geral == true){
  cf.config_geral();
  }

  if (pausar_jogo == true) {
    ge.pausar_jogo();
  } else {
  }

  fill(255);

  fill(0);
  textSize(16);
  text("Por: Adrião Morais, Carla Semedo, Elton Gomes, Igor Solva e Ruben Barros - TMC 4º Ano", width/2, height-20);


if (show_splash_screen == true) {
    sp.splash_creen_content();
  }
  //debug
  debug = false;
  if (debug == true) {
    noStroke();
    fill(0);
    rectMode(CENTER);
    rect(width/2, height-30, width, 50);
    fill(255);
    text("X: "+ mouseX + "    Y:"+ mouseY, width/2, height-20);

    rectMode(CORNER);
  }

  //fill(verde_claro, 100);
  //rect(width/2-100, height-120, 300,50);
}
