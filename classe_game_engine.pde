class game_engine {


  int button_Size = width/2;
  void start_menu() {
    if (show_menu == true) {
      fill(255, 245);
      rect(100, 100, 300, 300);
      textSize(20);
      noStroke();
      fill(verde_escuro);

      // MENU INICIAL


      rect(0, 50, width, height);



      strokeWeight(1);
      stroke(255, 100);

      // BOTONS
      //BOTAO 1
      if (mouseX > width/4 && mouseX < width/4*3 && mouseY > height/10 && mouseY < height/10*2) {
        button_Size = 110;
        cursor(hand_cursor, 30, 30);
        textSize(22);
        strokeWeight(10);

        if (mousePressed == true) {
          game_start = true;
        }
      } else {
        cursor(ARROW);
        textSize(20);
        strokeWeight(1);
      }

      //height/10*2+height/20+20

      image(btn_menu, width/2-200, height/10, 400, height/8 );
      fill(#FF8200);
      text("PLAY", width/2, height/10+height/20+20);
      strokeWeight(1);


      //BOTAO 2
      if (mouseX > width/4 && mouseX < width/4*3 && mouseY > height/10*2+20 && mouseY < height/10*3+20) {
        button_Size = 110;
        cursor(hand_cursor, 30, 30);
        textSize(22);
        strokeWeight(10);

        if (mousePressed == true) {
          
          name_playerUm = "PLAYER UM";
          name_playerDois = "PLAYER DOIS";
          player_Dois_pontuacao = 0;
          player_Um_pontuacao = 0;
        }
      } else {
        cursor(ARROW);
        textSize(20);
      }



      image(btn_menu, width/2-200, height/9*2, 400, height/8 );
      fill(#FF8200);
      text("Reiniciar o Jogo", width/2, height/10*2+height/20+28);

      strokeWeight(1);


      //BOTAO 3
      if (mouseX > width/4 && mouseX < width/4*3 && mouseY > height/10*3+40 && mouseY < height/10*4+40) {
        button_Size = 110;
        cursor(hand_cursor, 30, 30);
        textSize(22);
        strokeWeight(10);

        if (mousePressed == true) {
         show_config_geral = true;

          //show_menu = false;
          show_config_geral = true;
        }
      } else {
        cursor(ARROW);
        textSize(20);
      }



      image(btn_menu, width/2-200, height/9*3+8, 400, height/8 );
      fill(#FF8200);
      text("CONFIGURAÇÕES", width/2, height/10*3+height/20+48);
      strokeWeight(1);


      //BOTAO 4
      if (mouseX > width/4 && mouseX < width/4*3 && mouseY > height-200) {
        button_Size = 110;
        cursor(hand_cursor, 30, 30);
        textSize(22);
        strokeWeight(10);

        if (mousePressed == true) {
          exit();
        }
      } else {
        cursor(ARROW);
        textSize(20);
      }


      image(btn_menu, width/2-200, height-200, 400, height/8 );
      fill(#FF8200);
      text("SAIR", width/2, height-135);
      strokeWeight(1);



      strokeWeight(1);
      rectMode(CORNER);
    }
  }

  void start_jogo() {
    player_trilha.loop();
  }



  void pausar_jogo() {
    stroke(creme);

    // botao continuar
    fill(255);
    rect(width/2-150, height-100, 300, 50, 10);
    fill(verde_escuro);
    textSize(30);
    text("CONTINUAR", width/2, height-66);


    if (mouseX > width/2-100 && mouseX < width/2+100 && mouseY > height-100 && mouseY < height-50) {


      // botao continuar hover

      strokeWeight(5);
      fill(verde_escuro);
      rect(width/2-150, height-100, 300, 50, 10);

      strokeWeight(0.5);
      fill(255);
      textSize(30);
      text("CONTINUAR", width/2, height-66);
      cursor(hand_cursor, 30, 30);

      // Ao se concluir uma jogada os valores são resetados exeto as pontuações
      if (mousePressed == true ) {
        pausar_jogo = false;
        if (player_trilha_on == true) {
          player_trilha.loop();
        }

        player_audiofx1.rewind();


        // Reset selected casa
        selected_casa = 0;
        // Reset used casa
        casa_1_com_player = 0;
        casa_2_com_player = 0;
        casa_3_com_player = 0;
        casa_4_com_player = 0;
        casa_5_com_player = 0;
        casa_6_com_player = 0;
        casa_7_com_player = 0;
        casa_8_com_player = 0;
        casa_9_com_player = 0;

        // RESET PLAYER POSITION
        p1x = 50;
        p1y = 190;
        p2x = 50;
        p2y = 250;
        p3x = 50;
        p3y = 310;


        p4x = 450;
        pay = 190;
        p5x = 450;
        pby = 250;
        p6x = 450;
        pcy = 310;

        pontuacao_efetuado = false;
      }
    }
  }
  void player_time(int selected_player) {
    if (pausar_jogo != true) {

      if (selected_player == 1) {
        fill(verde_claro);
        rect(15, 100, 250, 40, 5);
      } else if (selected_player == 2) {
        fill(laranja);
        rect(width-265, 100, 250, 40, 5);
      }
    }
  }
}
