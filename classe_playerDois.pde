class playerDois {

  boolean playerDois_size_mais = true;
  int playerDois_size_animation = 50;


  float radius1 = 21;
  float radius2 = 26;
  int npoints = 14;

  void playerDois_pedra(float x_position, float y_position) {


    fill(creme);
    stroke(laranja);
    strokeWeight(8);
    int tl = translate_center;


    rect(tl + x_position-30, y_position-30, 50, 50, 10);
  }



  void animar_playerDois() {

    if (radius1 >= 18 && radius1 <= 22) {
      playerDois_size_mais = true;
    } else {
      playerDois_size_mais = false;
    }
    if (selected_player == 2) {
      if (playerDois_size_mais == true) {
        radius2 = radius2 + 2;
        radius1 = radius1 + 2;
      } else {
        radius2 = radius2 - 2;
        radius1 = radius1 - 2;
      }
    }
  }
  // TITULO
  void player_playerDois_title() {
    if (game_start == true) {
      textSize(16);
      fill(0);
      text(name_playerDois, width-150, 125);
    }
  }
}
