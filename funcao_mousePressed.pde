void mousePressed() {
  if (pausar_jogo != true) {
    //Selecionar a pedra a ser jogada
    if (mouseX > translate_center + p1x -25 && mouseX < translate_center + p1x + 25 && mouseY > p1y - 25 && mouseY < p1y + 25) {
      selected_pedra = 1;
    } else if (mouseX > translate_center + p2x -25 && mouseX < translate_center + p2x + 25 && mouseY > p2y - 25 && mouseY < p2y + 25) {
      selected_pedra = 2;
    } else if (mouseX > translate_center + p3x -25 && mouseX < translate_center + p3x + 25 && mouseY > p3y - 25 && mouseY < p3y + 25) {
      selected_pedra = 3;
    } else if (mouseX > translate_center + p4x -25 && mouseX < translate_center + p4x + 25 && mouseY > pay - 25 && mouseY < pay + 25) {
      selected_pedra = 4;
    } else if (mouseX > translate_center + p5x -25 && mouseX < translate_center + p5x + 25 && mouseY > pby - 25 && mouseY < pby + 25) {
      selected_pedra = 5;
    } else if (mouseX > translate_center + p6x -25 && mouseX < translate_center + p6x + 25 && mouseY > pcy - 25 && mouseY < pcy + 25) {
      selected_pedra = 6;
    }


    //Limpar registro de utilizacao de uma casa
    if (selected_pedra == 1) {

      if (casa_1_com_pedra == 1) {
        casa_1_com_player = 0;
        casa_1_com_pedra = 0;
      } else if (casa_2_com_pedra == 1) {
        casa_2_com_player = 0;
        casa_2_com_pedra = 0;
      } else if (casa_3_com_pedra == 1) {
        casa_3_com_player = 0;
        casa_3_com_pedra = 0;
      } else if (casa_4_com_pedra == 1) {
        casa_4_com_player = 0;
        casa_4_com_pedra = 0;
      } else if (casa_5_com_pedra == 1) {
        casa_5_com_player = 0;
        casa_5_com_pedra = 0;
      } else if (casa_6_com_pedra == 1) {
        casa_6_com_player = 0;
        casa_6_com_pedra = 0;
      } else if (casa_7_com_pedra == 1) {
        casa_7_com_player = 0;
        casa_7_com_pedra = 0;
      } else if (casa_8_com_pedra == 1) {
        casa_8_com_player = 0;
        casa_8_com_pedra = 0;
      } else if (casa_9_com_pedra == 1) {
        casa_9_com_player = 0;
        casa_9_com_pedra = 0;
      }
    } else if (selected_pedra == 2) {

      if (casa_1_com_pedra == 2) {
        casa_1_com_player = 0;
        casa_1_com_pedra = 0;
      } else if (casa_2_com_pedra == 2) {
        casa_2_com_player = 0;
        casa_2_com_pedra = 0;
      } else if (casa_3_com_pedra == 2) {
        casa_3_com_player = 0;
        casa_3_com_pedra = 0;
      } else if (casa_4_com_pedra == 2) {
        casa_4_com_player = 0;
        casa_4_com_pedra = 0;
      } else if (casa_5_com_pedra == 2) {
        casa_5_com_player = 0;
        casa_5_com_pedra = 0;
      } else if (casa_6_com_pedra == 2) {
        casa_6_com_player = 0;
        casa_6_com_pedra = 0;
      } else if (casa_7_com_pedra == 2) {
        casa_7_com_player = 0;
        casa_7_com_pedra = 0;
      } else if (casa_8_com_pedra == 2) {
        casa_8_com_player = 0;
        casa_8_com_pedra = 0;
      } else if (casa_9_com_pedra == 2) {
        casa_9_com_player = 0;
        casa_9_com_pedra = 0;
      }
    } else if (selected_pedra == 3) {

      if (casa_1_com_pedra == 3) {
        casa_1_com_player = 0;
        casa_1_com_pedra = 0;
      } else if (casa_2_com_pedra == 3) {
        casa_2_com_player = 0;
        casa_2_com_pedra = 0;
      } else if (casa_3_com_pedra == 3) {
        casa_3_com_player = 0;
        casa_3_com_pedra = 0;
      } else if (casa_4_com_pedra == 3) {
        casa_4_com_player = 0;
        casa_4_com_pedra = 0;
      } else if (casa_5_com_pedra == 3) {
        casa_5_com_player = 0;
        casa_5_com_pedra = 0;
      } else if (casa_6_com_pedra == 3) {
        casa_6_com_player = 0;
        casa_6_com_pedra = 0;
      } else if (casa_7_com_pedra == 3) {
        casa_7_com_player = 0;
        casa_7_com_pedra = 0;
      } else if (casa_8_com_pedra == 3) {
        casa_8_com_player = 0;
        casa_8_com_pedra = 0;
      } else if (casa_9_com_pedra == 3) {
        casa_9_com_player = 0;
        casa_9_com_pedra = 0;
      }
    } else if (selected_pedra == 4) {

      if (casa_1_com_pedra == 4) {
        casa_1_com_player = 0;
        casa_1_com_pedra = 0;
      } else if (casa_2_com_pedra == 4) {
        casa_2_com_player = 0;
        casa_2_com_pedra = 0;
      } else if (casa_3_com_pedra == 4) {
        casa_3_com_player = 0;
        casa_3_com_pedra = 0;
      } else if (casa_4_com_pedra == 4) {
        casa_4_com_player = 0;
        casa_4_com_pedra = 0;
      } else if (casa_5_com_pedra == 4) {
        casa_5_com_player = 0;
        casa_5_com_pedra = 0;
      } else if (casa_6_com_pedra == 4) {
        casa_6_com_player = 0;
        casa_6_com_pedra = 0;
      } else if (casa_7_com_pedra == 4) {
        casa_7_com_player = 0;
        casa_7_com_pedra = 0;
      } else if (casa_8_com_pedra == 4) {
        casa_8_com_player = 0;
        casa_8_com_pedra = 0;
      } else if (casa_9_com_pedra == 4) {
        casa_9_com_player = 0;
        casa_9_com_pedra = 0;
      }
    } else if (selected_pedra == 5) {

      if (casa_1_com_pedra == 5) {
        casa_1_com_player = 0;
        casa_1_com_pedra = 0;
      } else if (casa_2_com_pedra == 5) {
        casa_2_com_player = 0;
        casa_2_com_pedra = 0;
      } else if (casa_3_com_pedra == 5) {
        casa_3_com_player = 0;
        casa_3_com_pedra = 0;
      } else if (casa_4_com_pedra == 5) {
        casa_4_com_player = 0;
        casa_4_com_pedra = 0;
      } else if (casa_5_com_pedra == 5) {
        casa_5_com_player = 0;
        casa_5_com_pedra = 0;
      } else if (casa_6_com_pedra == 5) {
        casa_6_com_player = 0;
        casa_6_com_pedra = 0;
      } else if (casa_7_com_pedra == 5) {
        casa_7_com_player = 0;
        casa_7_com_pedra = 0;
      } else if (casa_8_com_pedra == 5) {
        casa_8_com_player = 0;
        casa_8_com_pedra = 0;
      } else if (casa_9_com_pedra == 5) {
        casa_9_com_player = 0;
        casa_9_com_pedra = 0;
      }
    } else if (selected_pedra == 6) {

      if (casa_1_com_pedra == 6) {
        casa_1_com_player = 0;
        casa_1_com_pedra = 0;
      } else if (casa_2_com_pedra == 6) {
        casa_2_com_player = 0;
        casa_2_com_pedra = 0;
      } else if (casa_3_com_pedra == 6) {
        casa_3_com_player = 0;
        casa_3_com_pedra = 0;
      } else if (casa_4_com_pedra == 6) {
        casa_4_com_player = 0;
        casa_4_com_pedra = 0;
      } else if (casa_5_com_pedra == 6) {
        casa_5_com_player = 0;
        casa_5_com_pedra = 0;
      } else if (casa_6_com_pedra == 6) {
        casa_6_com_player = 0;
        casa_6_com_pedra = 0;
      } else if (casa_7_com_pedra == 6) {
        casa_7_com_player = 0;
        casa_7_com_pedra = 0;
      } else if (casa_8_com_pedra == 6) {
        casa_8_com_player = 0;
        casa_8_com_pedra = 0;
      } else if (casa_9_com_pedra == 6) {
        casa_9_com_player = 0;
        casa_9_com_pedra = 0;
      }
    }
  //Calcular o offset - o valor de correção da posição de cada pedra caso, ao carrega-lo o rato não estiver no centro da mesma
  switch(selected_pedra) {
  case 1:
    if (selected_player == 1) {
      xOffset = mouseX-p1x;
      yOffset = mouseY-p1y;
    }
    break;
  case 2:
    if (selected_player == 1) {
      xOffset = mouseX-p2x;
      yOffset = mouseY-p2y;
    }
    break;
  case 3:
    if (selected_player == 1) {
      xOffset = mouseX-p3x;
      yOffset = mouseY-p3y;
    }
    break;
  case 4:
    xOffset = mouseX-p4x;
    yOffset = mouseY-pay;
    break;
  case 5:
    xOffset = mouseX-p5x;
    yOffset = mouseY-pby;
    break;
  case 6:
    xOffset = mouseX-p6x;
    yOffset = mouseY-pcy;
    break;
  }

 
}
}
