class extrutura {



  void  topBar() {
    textAlign(CENTER);
    textSize(30);
    fill(verde_escuro);
    noStroke();
    rect(0, 0, width, 55);
    fill(0);
    text("FRIC FRAC", width/2, 40);





    if (game_start == true) {
      //BUTTONS MENU
      fill(#00EECD);    
      
      rect(width-60, 10, 35, 7, 2);
      rect(width-60, 22, 35, 7, 2);
      rect(width-60, 34, 35, 7, 2);

      //BUTTONS CONFIG
      image(icon_config, width-150, 10, 40, 40 );
    }
    if (mouseX > width-60 && mouseX < width && mouseY < 48 && mousePressed) {
      game_start = false;
    }

    if (mouseX > width-150 && mouseX < width - 100 && mouseY < 48 && mousePressed) {
      show_menu = false;
      show_config_geral = true;
    }
  }

  void linhas_casas() {
    noFill();
    stroke(255);
    for (int casa_v = translate_center+100; casa_v < translate_center+400; casa_v = casa_v + 100) {
      for (int casa_h = 100; casa_h < 400; casa_h = casa_h + 100) {
        rect(casa_v, casa_h, 100, 100);
      }
    }
    strokeWeight(8);
    stroke(0);
    line(translate_center + 200, 100, translate_center + 200, 400);
    line(translate_center + 300, 100, translate_center + 300, 400);
    line(translate_center + 100, 200, translate_center + 400, 200);
    line(translate_center + 100, 300, translate_center + 400, 300);

    strokeWeight(0);
    stroke(0);

    if (pausar_jogo != true) {
      //Identificar a casa com mouse Hover
      fill(verde_claro, 50);

      if (selected_casa == 1) {
        rect(translate_center+100, 100, 100, 100);
      } else if (selected_casa == 2) {
        rect(translate_center+200, 100, 100, 100);
      } else if (selected_casa == 3) {
        rect(translate_center+300, 100, 100, 100);
      } else if (selected_casa == 4) {
        rect(translate_center+100, 200, 100, 100);
      } else if (selected_casa == 5) {
        rect(translate_center+200, 200, 100, 100);
      } else if (selected_casa == 6) {
        rect(translate_center+300, 200, 100, 100);
      } else if (selected_casa == 7) {
        rect(translate_center+100, 300, 100, 100);
      } else if (selected_casa == 8) {
        rect(translate_center+200, 300, 100, 100);
      } else if (selected_casa == 9) {
        rect(translate_center+300, 300, 100, 100);
      }

      //line();
    }

    noFill();
  }
}
