class pontuacao {

  // CASO 1 -----------------------

  void calc_pontuacao() {

    if (pontuacao_efetuado == false) {
      //CASO 1 - Se as casas 1, 2 e 3 estiverem ocupadas com pedras do jogador 1 (playerUm) é ponto para o jogador 1
      // Se o player 1 for o ganhador
      if (
        casa_1_com_player == 1 && casa_2_com_player == 1 && casa_3_com_player == 1 ||
        casa_1_com_player == 2 && casa_2_com_player == 2 && casa_3_com_player == 2) {

        pontuacao_efetuado = true;
        pausar_jogo = true;
        nova_jogada = true;

        //Definir o player ganhador como Selected player
        if (selected_player == 1 ) {
          //Calcular pontuacao do player gangador
          if (pontuacao_efetuado == true) {
            player_Um_pontuacao = player_Um_pontuacao + 1;
          }
          selected_player = 2;
        } else if (selected_player == 2 ) {
          //Calcular pontuacao do player gangador
          if (pontuacao_efetuado == true) {
            player_Dois_pontuacao = player_Dois_pontuacao + 1;
          }
          selected_player = 1;
        }


        //Apontar o caso que gerou a pontuação
        caso_de_pontuacao = 1;


        // Se o player 2 for o ganhador
      } else

        // CASO 2 ---------------------------------------------------------------------------------------------------------------------
        // Se o player 1 for o ganhador
        if (casa_4_com_player == 1 && casa_5_com_player == 1 && casa_6_com_player == 1 ) {

          pontuacao_efetuado = true;
          pausar_jogo = true;
          nova_jogada = true;

          //Definir o player ganhador como Selected player
          //if (selected_player == 1 ) {
          //  selected_player = 2;
          //} else if (selected_player == 2 ) {
          //  selected_player = 1;
          //}

          //Calcular pontuacao do player gangador
          if (pontuacao_efetuado == true) {
            player_Um_pontuacao = player_Um_pontuacao + 1;
          }
          //Apontar o caso que gerou a pontuação
          caso_de_pontuacao = 2;

          // Se o player 2 for o ganhador
        } else if (casa_4_com_player == 2 && casa_5_com_player == 2 && casa_6_com_player == 2 ) {

          pontuacao_efetuado = true;
          pausar_jogo = true;
          nova_jogada = true;

          //Definir o player ganhador como Selected player
          //if (selected_player == 1 ) {
          //  selected_player = 2;
          //} else if (selected_player == 2 ) {
          //  selected_player = 1;
          //}

          //Calcular pontuacao do player gangador
          if (pontuacao_efetuado == true) {
            player_Dois_pontuacao = player_Dois_pontuacao + 1;
          }
          //Apontar o caso que gerou a pontuação
          caso_de_pontuacao = 2;
        } else

          // CASO 3 ---------------------------------------------------------------------------------------------------------------------
          // Se o player 1 for o ganhador
          if (casa_7_com_player == 1 && casa_8_com_player == 1 && casa_9_com_player == 1 ) {

            pontuacao_efetuado = true;
            pausar_jogo = true;
            nova_jogada = true;

            //Definir o player ganhador como Selected player
            //if (selected_player == 1 ) {
            //  selected_player = 2;
            //} else if (selected_player == 2 ) {
            //  selected_player = 1;
            //}

            //Calcular pontuacao do player gangador
            if (pontuacao_efetuado == true) {
              player_Um_pontuacao = player_Um_pontuacao + 1;
            }
            //Apontar o caso que gerou a pontuação
            caso_de_pontuacao = 3;

            // Se o player 2 for o ganhador
          } else if (casa_7_com_player == 2 && casa_8_com_player == 2 && casa_9_com_player == 2 ) {

            pontuacao_efetuado = true;
            pausar_jogo = true;
            nova_jogada = true;

            //Definir o player ganhador como Selected player
            //if (selected_player == 1 ) {
            //  selected_player = 2;
            //} else if (selected_player == 2 ) {
            //  selected_player = 1;
            //}

            //Calcular pontuacao do player gangador
            if (pontuacao_efetuado == true) {
              player_Dois_pontuacao = player_Dois_pontuacao + 1;
            }
            //Apontar o caso que gerou a pontuação
            caso_de_pontuacao = 3;
          } else

            // CASO 4 ---------------------------------------------------------------------------------------------------------------------
            // Se o player 1 for o ganhador
            if (casa_1_com_player == 1 && casa_4_com_player == 1 && casa_7_com_player == 1 ) {

              pontuacao_efetuado = true;
              pausar_jogo = true;
              nova_jogada = true;

              //Definir o player ganhador como Selected player
              //if (selected_player == 1 ) {
              //  selected_player = 2;
              //} else if (selected_player == 2 ) {
              //  selected_player = 1;
              //}

              //Calcular pontuacao do player gangador
              if (pontuacao_efetuado == true) {
                player_Um_pontuacao = player_Um_pontuacao + 1;
              }
              //Apontar o caso que gerou a pontuação
              caso_de_pontuacao = 4;

              // Se o player 2 for o ganhador
            } else if (casa_1_com_player == 2 && casa_4_com_player == 2 && casa_7_com_player == 2 ) {

              pontuacao_efetuado = true;
              pausar_jogo = true;
              nova_jogada = true;

              //Definir o player ganhador como Selected player
              //if (selected_player == 1 ) {
              //  selected_player = 2;
              //} else if (selected_player == 2 ) {
              //  selected_player = 1;
              //}

              //Calcular pontuacao do player gangador
              if (pontuacao_efetuado == true) {
                player_Dois_pontuacao = player_Dois_pontuacao + 1;
              }
              //Apontar o caso que gerou a pontuação
              caso_de_pontuacao = 4;
            } else

              // CASO 5 ---------------------------------------------------------------------------------------------------------------------
              // Se o player 1 for o ganhador
              if (casa_2_com_player == 1 && casa_5_com_player == 1 && casa_8_com_player == 1 ) {

                pontuacao_efetuado = true;
                pausar_jogo = true;
                nova_jogada = true;

                //Definir o player ganhador como Selected player
                //if (selected_player == 1 ) {
                //  selected_player = 2;
                //} else if (selected_player == 2 ) {
                //  selected_player = 1;
                //}

                //Calcular pontuacao do player gangador
                if (pontuacao_efetuado == true) {
                  player_Um_pontuacao = player_Um_pontuacao + 1;
                }
                //Apontar o caso que gerou a pontuação
                caso_de_pontuacao = 5;

                // Se o player 2 for o ganhador
              } else if (casa_2_com_player == 2 && casa_5_com_player == 2 && casa_8_com_player == 2 ) {

                pontuacao_efetuado = true;
                pausar_jogo = true;
                nova_jogada = true;

                //Definir o player ganhador como Selected player
                //if (selected_player == 1 ) {
                //  selected_player = 2;
                //} else if (selected_player == 2 ) {
                //  selected_player = 1;
                //}

                //Calcular pontuacao do player gangador
                if (pontuacao_efetuado == true) {
                  player_Dois_pontuacao = player_Dois_pontuacao + 1;
                }
                //Apontar o caso que gerou a pontuação
                caso_de_pontuacao = 5;
              } else

                // CASO 6 ---------------------------------------------------------------------------------------------------------------------
                // Se o player 1 for o ganhador
                if (casa_3_com_player == 1 && casa_6_com_player == 1 && casa_9_com_player == 1 ) {

                  pontuacao_efetuado = true;
                  pausar_jogo = true;
                  nova_jogada = true;

                  //Definir o player ganhador como Selected player
                  if (selected_player == 1 ) {
                    selected_player = 2;
                  } else if (selected_player == 2 ) {
                    selected_player = 1;
                  }

                  //Calcular pontuacao do player gangador
                  if (pontuacao_efetuado == true) {
                    player_Um_pontuacao = player_Um_pontuacao + 1;
                  }
                  //Apontar o caso que gerou a pontuação
                  caso_de_pontuacao = 6;

                  // Se o player 2 for o ganhador
                } else if (casa_3_com_player == 2 && casa_6_com_player == 2 && casa_9_com_player == 2 ) {

                  pontuacao_efetuado = true;
                  pausar_jogo = true;
                  nova_jogada = true;

                  //Definir o player ganhador como Selected player
                  //if (selected_player == 1 ) {
                  //  selected_player = 2;
                  //} else if (selected_player == 2 ) {
                  //  selected_player = 1;
                  //}

                  //Calcular pontuacao do player gangador
                  if (pontuacao_efetuado == true) {
                    player_Dois_pontuacao = player_Dois_pontuacao + 1;
                  }
                  //Apontar o caso que gerou a pontuação
                  caso_de_pontuacao = 6;
                } else

                  // CASO 7 ---------------------------------------------------------------------------------------------------------------------
                  // Se o player 1 for o ganhador
                  if (casa_1_com_player == 1 && casa_5_com_player == 1 && casa_9_com_player == 1 ) {

                    pontuacao_efetuado = true;
                    pausar_jogo = true;
                    nova_jogada = true;

                    //Definir o player ganhador como Selected player
                    //if (selected_player == 1 ) {
                    //  selected_player = 2;
                    //} else if (selected_player == 2 ) {
                    //  selected_player = 1;
                    //}

                    //Calcular pontuacao do player gangador
                    if (pontuacao_efetuado == true) {
                      player_Um_pontuacao = player_Um_pontuacao + 1;
                    }
                    //Apontar o caso que gerou a pontuação
                    caso_de_pontuacao = 7;

                    // Se o player 2 for o ganhador
                  } else if (casa_1_com_player == 2 && casa_5_com_player == 2 && casa_9_com_player == 2 ) {

                    pontuacao_efetuado = true;
                    pausar_jogo = true;
                    nova_jogada = true;

                    //Definir o player ganhador como Selected player
                    //if (selected_player == 1 ) {
                    //  selected_player = 2;
                    //} else if (selected_player == 2 ) {
                    //  selected_player = 1;
                    //}

                    //Calcular pontuacao do player gangador
                    if (pontuacao_efetuado == true) {
                      player_Dois_pontuacao = player_Dois_pontuacao + 1;
                    }
                    //Apontar o caso que gerou a pontuação
                    caso_de_pontuacao = 7;
                  } else

                    // CASO 8 ---------------------------------------------------------------------------------------------------------------------
                    // Se o player 1 for o ganhador
                    if (casa_3_com_player == 1 && casa_5_com_player == 1 && casa_7_com_player == 1 ) {

                      pontuacao_efetuado = true;
                      pausar_jogo = true;
                      nova_jogada = true;

                      //Definir o player ganhador como Selected player
                      //if (selected_player == 1 ) {
                      //  selected_player = 2;
                      //} else if (selected_player == 2 ) {
                      //  selected_player = 1;
                      //}

                      //Calcular pontuacao do player gangador
                      if (pontuacao_efetuado == true) {
                        player_Um_pontuacao = player_Um_pontuacao + 1;
                      }
                      //Apontar o caso que gerou a pontuação
                      caso_de_pontuacao = 8;

                      // Se o player 2 for o ganhador
                    } else if (casa_3_com_player == 2 && casa_5_com_player == 2 && casa_7_com_player == 2 ) {

                      pontuacao_efetuado = true;
                      pausar_jogo = true;
                      nova_jogada = true;

                      //Definir o player ganhador como Selected player
                      //if (selected_player == 1 ) {
                      //  selected_player = 2;
                      //} else if (selected_player == 2 ) {
                      //  selected_player = 1;
                      //}

                      //Calcular pontuacao do player gangador
                      if (pontuacao_efetuado == true) {
                        player_Dois_pontuacao = player_Dois_pontuacao + 1;
                      }
                      //Apontar o caso que gerou a pontuação
                      caso_de_pontuacao = 8;
                    }
    }

    if (game_start == true) {
      // EXIBIR PONTUACOES

      textSize(30);
      fill(0);

      // PLAYER UM
      text(player_Um_pontuacao, 230, 130);

      // PLAYER 2
      text(player_Dois_pontuacao, width-50, 130);
    }

    // SINALIZAR CASAS PONTUADAS
    if (pontuacao_efetuado == true) {

      player_trilha.pause();
      player_audiofx1.play();

      fill(verde_claro, 50);
      strokeWeight(10);
      stroke(laranja);


      switch(caso_de_pontuacao) {
        //casas na horizontal
      case 1:
        rect(translate_center+100, 100, 300, 100);
        break;
      case 2:
        rect(translate_center+100, 200, 300, 100);
        break;
      case 3:
        rect(translate_center+100, 300, 300, 100);
        break;
        //Casas va vertical
      case 4:
        rect(translate_center+100, 100, 100, 300);
        break;
      case 5:
        rect(translate_center+200, 100, 100, 300);
        break;
      case 6:
        rect(translate_center+300, 100, 100, 300);
        break;
      case 7:

        //casas na diagonal
        rect(translate_center+100, 100, 100, 100);
        rect(translate_center+200, 200, 100, 100);
        rect(translate_center+300, 300, 100, 100);
        break;
      case 8:
        rect(translate_center+300, 100, 100, 100);
        rect(translate_center+200, 200, 100, 100);
        rect(translate_center+100, 300, 100, 100);
        break;
      }

      strokeWeight(1);
    }
  }
}
