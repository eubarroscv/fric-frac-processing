class splash_creen{
  void splash_creen_content(){
    fill(verde_escuro);
    rect(0, 0, width, height);
    
    
     // TITLE
    textAlign(CENTER);
    textSize(100);
    fill(0);
    strokeWeight(100);
    stroke(255, 0, 0);
    text("FRIC FRAC", width/2, height/2-50);
    textAlign(LEFT);

    //Autor
    noStroke();
    textSize(20);
    textAlign(CENTER);
    text("Por: Adrião Morais, Carla Semedo, Elton Gomes, Igor Solva e Ruben Barros", width/2, height/2);
    textSize(20);
    text("Curso Tecnologias Multimédia e Comunicação", width/2, height/2 + 30);

    if (i < 100) {
      i = i+1;
    }

    if (i >= 100) {
      show_config_inicial = true;
      show_splash_screen = false;
      i = 0;
    }
    strokeWeight(2);
    stroke(255);
    noFill();
    rect(width/2-150, height-200, 300, 50, 10);
    fill(verde_claro);
    rect(width/2-150, height-200, i*3, 50, 10);
    fill(0);
    text(""+i+"%", width/2, height-168);
    
    
    
  }
}
